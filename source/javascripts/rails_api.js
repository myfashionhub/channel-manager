window.RailsApi = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    var channelRouter = new RailsApi.Routers.Channels();
    Backbone.history.start({});
  }
};

window.baseUrl = 'http://'+window.location.hostname+':3001/api';
window.apiKey;

var keys = {
  staging: '',
  dev: '',
  production: '',
  heroku: 'a5136852b05c69f652a5a4cd5c2aa8b1'
};

if (baseUrl.indexOf('staging') > -1) {
  window.apiKey = keys.staging;
} else if (baseUrl.indexOf('nessa.dev') > -1 || baseUrl.indexOf('localhost') > -1) {
  window.apiKey = keys.dev;
} else if (baseUrl.indexOf('herokuapp') > -1) {
  window.baseUrl = 'http://channel-api.herokuapp.com/api'
  window.apiKey = keys.heroku;
} else {
  window.apiKey = keys.production;
}
