# Waywire Channel Manager
Client-side app using the Channel Manager API

### Getting started
* Clone the repository, run `bundle install`
* To start the app: `middleman server`
* Plug in the appropriate API url (window.baseUrl) in `source/javascripts/rails_api.js`
* Obtain API token for `window.apiKey` in `rails_api.js`
* For more info checkout [Middleman docs](https://middlemanapp.com/)
